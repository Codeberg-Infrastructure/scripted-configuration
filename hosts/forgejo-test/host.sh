#!/usr/bin/env bash

set -ex

if [ ! -d /srv/ceph/data/staging ]; then
       mkdir -p /srv/ceph/data/staging
fi

MEM=8
CPU=2
WEIGHT=20
LXC_CUSTOM=$(cat <<EOF
lxc.mount.entry = /dev/fuse dev/fuse none bind,create=file,rw 0 0
lxc.mount.entry = /srv/ceph/data/staging data/ceph none defaults,bind,create=dir 0 0
security.nesting = true
EOF
)
