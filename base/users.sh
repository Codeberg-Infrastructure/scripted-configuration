#!/usr/bin/env bash

set -ex

function user_fetch_sshkey () {
	USERNAME="${1}"
	SSHKEY=$(grep "(${USERNAME})" ./hosts/_host/var/jump/.ssh/authorized_keys)
	echo "${SSHKEY}"
}

function user_setup () {
	USERNAME="${1}"
	USER_GROUPS_NEW="${2}"
	
	# check if user does not exist yet and create
	if [ "$(id -u "${USERNAME}" 2>/dev/null)" == "" ]; then
		adduser "${USERNAME}"
	fi
	sudo -u "${USERNAME}" mkdir -p "/home/${USERNAME}/.ssh"
	
	USER_GROUPS_EXISTING=$(groups "${USERNAME}")
	if [ ! "${USER_GROUPS_EXISTING}" == "${USERNAME} ${USER_GROUPS_NEW}" ] && [ "${USER_GROUPS_NEW}" != "" ]; then
		usermod "${USERNAME}" -G "${USER_GROUPS_NEW}"
	fi

	SSHKEY=$(user_fetch_sshkey "${USERNAME}")
	echo "${SSHKEY}" | sudo -u "${USERNAME}" tee "/home/${USERNAME}/.ssh/authorized_keys"

	# colourful bash prompt
	source base/base.sh && line_in_file "/home/${USERNAME}/.bashrc" "[ $(tput colors 2>/dev/null || echo 0) -le 1 ] || PS1='\\[\\e[1;38;5;244m\\]\\t \\[\\e[1;36m\\]\\u@\\H \\[\\e[1;33m\\]\\w \\[\\e[1;36m\\]\\$ \\[\\e[0m\\]'"
}

function user_grant () {
	TARGET_USER="${1}"
	USERNAME="${2}"

	if [ "${TARGET_USER}" == "root" ]; then
		TARGET_HOME="/root"
		sed 's/PermitRootLogin no/PermitRootLogin yes/g' -i "/etc/ssh/sshd_config.d/20-codeberg-default.conf"
	else
		TARGET_HOME="/home/${TARGET_USER}"
	fi

	if [ ! -d "${TARGET_HOME}/.ssh" ]; then
		mkdir -p "${TARGET_HOME}/.ssh"
	fi

	SSHKEY=$(user_fetch_sshkey "${USERNAME}")
	echo "${SSHKEY}" >> "${TARGET_HOME}/.ssh/authorized_keys"
}

function user_setup_jump () {
	# check if user does not exist yet and create
	if [ "$(id -u jump 2>/dev/null)" == "" ]; then
		useradd --create-home --user-group --shell /usr/sbin/nologin --system -d /var/jump jump
	fi

	# this is to match the current setup on kampenwand
	mkdir -p /var/jump/.ssh
	chmod 755 /var/jump -R
	chown jump:root /var/jump
	chown jump.jump /var/jump/.ssh -R

	cat > /etc/ssh/sshd_config.d/30-jump-user.conf <<EOF
Match User jump
  PermitTTY no
  X11Forwarding no
  PermitTunnel no
  GatewayPorts no
  ForceCommand /usr/sbin/nologin
EOF
	# sync authorized users file
	source base/base.sh && install_file "_host" "/var/jump/.ssh/authorized_keys"
}
