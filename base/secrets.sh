#!/usr/bin/env bash
set -ex

if [ -f "/root/.secrets" ]; then
	source "/root/.secrets"
else
	touch "/root/.secrets"
fi

# This will get the secret if defined, otherwise ask and store.
secret_get () {
	# ${!VAR} allows to reference a variable in a variable (this case the VAR variable).
	# Check if the provided string is defined as a enviroment.
	if [ -z ${!1+x} ]; then
		# Ask the user user to provide a value. We store it at
		# provided environment, so it can be immediatly used by the caller.
		read -p "Please provide a value for $1: " ${1}
		# Store it.
		echo "$1='${!1}'" >> /root/.secrets
	fi
}
