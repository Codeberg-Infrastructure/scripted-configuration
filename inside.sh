#!/usr/bin/env bash

set -ex

# define constants
HOSTNAME=$(hostname -s)
SCRIPT_LOCATION="/root/scripted-configuration.git"
cd "${SCRIPT_LOCATION}"

if grep -q "ID=debian" /etc/os-release; then
	# install packages
	apt-get update && apt-get upgrade -y
	apt-get -y install sudo vim nano joe curl wget unzip git man-db htop iotop dnsutils tmux bash-completion
	apt-get -y install unattended-upgrades
fi

git config --global user.email "contact@codeberg.org"
git config --global user.name "Codeberg Admins @ ${HOSTNAME}"

# shellcheck disable=SC1091
source base/base.sh

line_in_file /root/.bashrc "[ $(tput colors 2>/dev/null || echo 0) -le 1 ] || PS1='\\[\\e[1;38;5;244m\\]\\t \\[\\e[1;36m\\]\\u@\\H \\[\\e[1;33m\\]\\w \\[\\e[1;36m\\]\\$ \\[\\e[0m\\]'"

# enforce log rotation on all hosts
# shellcheck disable=SC1091
source base/systemd.sh
setup_journald

# execute more setups found via
# hosts/HOSTNAME.inside.sh or
# hosts/HOSTNAME/inside.sh

# shellcheck source=hosts/example_hostname.inside.sh
if [ -f "hosts/${HOSTNAME}.inside.sh" ]; then
	# shellcheck disable=SC1091
	source "hosts/${HOSTNAME}.inside.sh"
fi
# shellcheck source=hosts/example_hostname/inside.sh
if [ -f "hosts/${HOSTNAME}/inside.sh" ]; then
	# shellcheck disable=SC1091
	source "hosts/${HOSTNAME}/inside.sh"
fi
