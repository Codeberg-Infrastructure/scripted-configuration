#!/usr/bin/env bash

set -ex


source "base/base.sh"
setup_sshd

source "base/users.sh"
user_grant "root" "earl"
user_grant "root" "crystal"
systemctl reload sshd

