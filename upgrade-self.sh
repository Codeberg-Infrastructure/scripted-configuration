#!/usr/bin/env bash

set -ex
source "base/base.sh"

if ! confirm "You are about to perform a system upgrade from Debian Bookworm (12) to Debian Trixie (13). Do you want to proceed?"; then exit 1; fi

sed -i "s/bookworm/trixie/g" /etc/apt/sources.list

if [ "$(ls -A /etc/apt/sources.list.d)" ]; then
	if ! confirm "Your /etc/apt/sources.list.d is not empty. We will not attempt to automatically replace versions there. Please check that your external sources are compatible and proceed afterwards. Proceed now?"; then exit 1; fi
fi

echo <<EOF
Upgrade notes:

DO NOT DO THIS BEFORE TRIXIE HAS BEEN RELEASED, OR YOU REALLY WANT TO TEST

If you are asked whether to replace /etc/ssh/sshd_config,
you can probably safely accept (unless you modified it yourself).
In the past, we used to overwrite this file,
but it is no longer necessary.

After the upgrade, you'll likely have dbus installed on your system.
It is not necessary, and you can safely purge it.
EOF


if ! confirm "Have you read and understood the upgrade notes?"; then exit 1; fi


apt-get update
apt-get upgrade --no-install-recommends --autoremove --purge
apt-get dist-upgrade --no-install-recommends --autoremove --purge
