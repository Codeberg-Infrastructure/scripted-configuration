#!/bin/sh
# Guarded because update-initramfs actually executes those!
if [ ! -f /etc/issue ]; then
ip link delete bond0
ip link set eno1 down
ip link set eno2 down
fi
