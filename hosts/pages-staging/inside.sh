#!/usr/bin/env bash

set -ex

source "base/users.sh"
setup_sshd

user_setup "pjs" "sudo"
user_setup "crapstone" "sudo"

user_grant "root" "pjs"
user_grant "root" "crapstone"
