#!/bin/sh
# Guarded because update-initramfs actually executes those!
if [ ! -f /etc/issue ]; then
ip link add bond0 type bond mode 802.3ad miimon 100 xmit_hash_policy layer3+4 updelay 300 downdelay 200
# the above does not set the bond mode properly in busybox, lets cheat here
echo "802.3ad" > /sys/class/net/bond0/bonding/mode
ip link set eno3 master bond0
ip link set eno3 up
ip link set eno4 master bond0
ip link set eno4 up
ip link set bond0 up
ip address add 217.197.91.161/26 dev bond0
ip route add default via 217.197.91.129 dev bond0
fi
