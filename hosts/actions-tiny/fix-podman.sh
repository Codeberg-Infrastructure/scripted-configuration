#!/usr/bin/env bash

systemctl stop runner
sudo -u runner bash -c 'cd; podman system reset --force'
sudo -u runner bash -c 'cd; podman system check'
systemctl restart runner-podman
sudo -u runner bash -c 'cd; podman system check'
systemctl start runner
