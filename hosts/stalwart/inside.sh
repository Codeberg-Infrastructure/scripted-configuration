#!/usr/bin/env bash

set -ex

source "base/users.sh"
source "base/base.sh"
setup_sshd

# ufw
setup_ufw
ufw allow 8080
ufw allow 25
ufw allow 25
ufw allow 465
ufw allow 993
ufw allow 587
ufw allow 143
