#!/usr/bin/env bash

set -ex

source "base/base.sh"
source "base/users.sh"

user_setup "otto" "sudo"
adduser --uid 3333  codeberg-backup || true

# ensure restic, rync and btrfs are present
apt_install_norecommends restic btrfs-progs rsync

apt_install_norecommends wireguard-tools vnstat

# backup disk snapshots
source "base/systemd.sh"
install_file "codeberg-backup" /root/snapshot-btrfs.sh
systemd_timer "snapshot btrfs" "*-*-* 02:20:19" "/root/snapshot-btrfs.sh" "root"

