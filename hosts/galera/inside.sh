#!/usr/bin/env bash

set -ex

source "base/users.sh"

user_setup "otto" "sudo"
user_setup "ashimokawa" "sudo"

source "base/base.sh"
setup_sshd

source "base/secrets.sh"
secret_get "GALERA_SST_SHARED_SECRET"
export GALERA_SST_SHARED_SECRET

apt-get install -y --no-install-recommends mariadb-server mariadb-backup

install_template "galera" "/etc/mysql/mariadb.conf.d/60-codeberg.cnf" || service mariadb restart
