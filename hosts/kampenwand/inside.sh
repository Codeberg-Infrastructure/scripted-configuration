#!/usr/bin/env bash

set -ex

source "base/users.sh"
user_setup "gusted" "sudo"
user_setup "ashimokawa" "sudo"
user_setup "crapstone" "sudo"

apt-get install -y firmware-bnx2x
apt-get install -y smartmontools
apt-get install -y --no-install-recommends haproxy 

source "base/secrets.sh"
secret_get "HAPROXY_BASICAUTH_MONITOR"
export HAPROXY_BASICAUTH_MONITOR
secret_get "HAPROXY_BASICAUTH_MIGRATION"
export HAPROXY_BASICAUTH_MIGRATION
secret_get "HAPROXY_BASICAUTH_ADMIN"
export HAPROXY_BASICAUTH_ADMIN
install_file "kampenwand" "/etc/haproxy/ratelimit-whitelist.acl"
install_template "kampenwand" "/etc/haproxy/haproxy.cfg" || (haproxy -c -f /etc/haproxy/haproxy.cfg && service haproxy restart)

# sync authorized users file
install_file "_host" "/var/jump/.ssh/authorized_keys"

# setup
apt-get install -y --no-install-recommends qemu-system libvirt-clients libvirt-daemon-system virtinst
install_file "kampenwand" "/etc/libvirt/qemu/networks/default.xml"
virsh net-autostart default
virsh net-list --name | grep -xF "default" || virsh net-start default

# network and lxc
apt-get install -y lxc dnsmasq bird2
apt-get install -y chrony # NTP

# lxc ipv6
install_file "kampenwand" /etc/default/lxc-net

# needed for bonding support via /etc/network/interfaces
apt-get install -y ifenslave
install_file "kampenwand" "/etc/hosts"
install_file "kampenwand" "/etc/network/interfaces"
install_file "_machines" "/etc/network/interfaces.d/vip.conf"
install_file_v2 "_machines" "/etc/sysctl.d/10-codeberg.conf" || sysctl -p /etc/sysctl.d/10-codeberg.conf

install_file "_machines" "/etc/resolv.conf"
install_file_v2 "_machines" "/etc/dnsmasq.d/codeberg.conf" || systemctl restart dnsmasq

# bird dynamic routing
install_file_v2 "kampenwand" "/etc/bird/bird.conf" || birdc configure
systemctl enable bird


# do not use setup_ufw yet because it is not yet fully managend by scripted-configuration
ufw allow in on lxcbr0
ufw route allow in on lxcbr0
ufw route allow out on lxcbr0
ufw allow in on enp5s0f0 to any port 53 # dns
ufw allow in on enp5s0f1 to any port 53 # dns
ufw allow in on wg-wobcom+ to any port 53 # dns
ufw allow in on enp5s0f0 to any port 3784 proto udp # bfd
ufw allow in on enp5s0f1 to any port 3784 proto udp # bfd
ufw allow in on enp5s0f0 to any port 6789 proto tcp # ceph mon
ufw allow in on enp5s0f1 to any port 6789 proto tcp # ceph mon
ufw allow in on enp5s0f0 to any port 3300 proto tcp # ceph mon v2
ufw allow in on enp5s0f1 to any port 3300 proto tcp # ceph mon v2
ufw allow in on enp5s0f0 to any port 6800:7300 proto tcp # ceph osd
ufw allow in on enp5s0f1 to any port 6800:7300 proto tcp # ceph osd
ufw allow in on enp5s0f0 to any port 3000 proto tcp # ceph grafana
ufw allow in on enp5s0f1 to any port 3000 proto tcp # ceph grafana
ufw allow in on enp5s0f0 to any port 8443 proto tcp # ceph dashboard
ufw allow in on enp5s0f1 to any port 8443 proto tcp # ceph dashboard
ufw allow in on enp5s0f0 to any port 9100 proto tcp # ceph node-exporter
ufw allow in on enp5s0f1 to any port 9100 proto tcp # ceph node-exporter
ufw allow in on enp5s0f0 to any port 9283 proto tcp # ceph prometheus exporter
ufw allow in on enp5s0f1 to any port 9283 proto tcp # ceph prometheus exporter
ufw allow in on enp5s0f0 to any port 8765 proto tcp # ceph cephadm discovery service
ufw allow in on enp5s0f1 to any port 8765 proto tcp # ceph cephadm discovery service
ufw allow in on enp5s0f0 to any port 9003:9005 proto tcp # ceph prometheus
ufw allow in on enp5s0f1 to any port 9003:9005 proto tcp # ceph prometheus

install_file_v2 "kampenwand" "/etc/ufw/before.rules" || ufw reload
install_file_v2 "kampenwand" "/etc/ufw/after.rules" || ufw reload

# fstab and mountpoints
mkdir -p /mnt/ceph-cluster
install_file_v2 "kampenwand" "/etc/fstab" || systemctl daemon-reload

# ceph dependecies
apt-get install -y podman lvm2
# ceph userspace tools
apt_install extrepo
extrepo enable ceph
apt-get update
apt-get install -y ceph-common


# crypt unlock
apt-get install -y dropbear-initramfs
install_file "kampenwand" "/etc/dropbear/initramfs/authorized_keys"
line_in_file "/etc/dropbear/initramfs/dropbear.conf" "DROPBEAR_OPTIONS=\"-I 180 -j -k -p 2222 -s -c cryptroot-unlock\""

install_file "kampenwand" "/etc/initramfs-tools/scripts/init-premount/bonding.sh"
install_file "kampenwand" "/etc/initramfs-tools/scripts/init-bottom/bonding.sh"
line_in_file "/etc/initramfs-tools/initramfs.conf" "IP=127.0.0.1:::::lo:off" # Just a dummy to make dropbear want to start :/
line_in_file "/etc/initramfs-tools/modules" "bonding"

if confirm "Do you want to update initram now? (It might take a time and is not necessary on every deployment"; then
	update-initramfs -u
fi
