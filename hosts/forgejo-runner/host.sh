#!/usr/bin/env bash

set -ex

# create docker volume for the mountpoint
mkdir /var/lib/lxc/forgejo-runner/rootfs/var/lib/docker/volumes -p
mkdir /var/lib/lxc/forgejo-runner/rootfs/var/lib/docker/overlay2 -p

MEM=80
CPU=32
WEIGHT=10
LXC_CUSTOM=$(cat <<EOF
lxc.mount.entry = /dev/fuse dev/fuse none bind,create=file,rw 0 0
lxc.mount.entry = tmpfs var/lib/docker/volumes tmpfs defaults,nosuid 0 0
lxc.mount.entry = tmpfs var/lib/docker/overlay2 tmpfs defaults,nosuid 0 0
security.nesting = true
lxc.cap.drop =
EOF
)
