#!/bin/sh
# Guarded because update-initramfs actually executes those!
if [ ! -f /etc/issue ]; then
ip link delete bond0
ip link set eno3 down
ip link set eno4 down
fi
