#!/usr/bin/env bash

set -ex

source "base/users.sh"

# ensure git has id 1001
if [ "$(id -u "git" 2>/dev/null)" == "" ]; then
	useradd --uid 1001 git
	mkdir -p /home/git
	chown git:git /home/git -R
fi

source "base/base.sh"
setup_sshd
# apply some tuned SSH configuration for Forgejo
install_file "gitea-production" "/etc/ssh/sshd_config.d/30-codeberg-forgejo.conf"

user_grant "root" "otto"
user_grant "root" "ashimokawa"
user_grant "root" "hw"
user_grant "root" "gusted"
systemctl reload sshd

apt-get install -y --no-install-recommends git mariadb-client rsync redis-server

## redis setup
line_in_file /etc/redis/redis.conf "maxmemory 2gb"
line_in_file /etc/redis/redis.conf "maxmemory-policy allkeys-lru"
## done

mkdir -p /data/git
# mkdir -p /mnt/ceph-cluster
chown git:git /data/git
chown git:git /mnt/ceph-cluster


# custom renderers
apt-get install -y --no-install-recommends pandoc asciidoctor
line_in_file /etc/sudoers "git ALL=(nobody) NOPASSWD: /usr/bin/pandoc"
line_in_file /etc/sudoers "git ALL=(nobody) NOPASSWD: /usr/bin/asciidoctor"

# experiment: update niceness of running Git processes
source "base/systemd.sh"
install_file "gitea-production" "/usr/local/bin/git-renice.sh"
systemd_timer "renice git" "*-*-* *:*:00/5" "/usr/local/bin/git-renice.sh" "git"

install_file_v2 "gitea-production" "/etc/systemd/system/gitea.service" || systemctl enable gitea.service
