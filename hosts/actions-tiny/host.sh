#!/usr/bin/env bash

set -ex

MEM=25
CPU=8
WEIGHT=5
LXC_CUSTOM=$(cat <<EOF
lxc.cgroup2.devices.allow = c 10:200 rwm
lxc.mount.entry = /dev/net/tun dev/net/tun none bind,create=file
lxc.mount.entry = tmpfs home/runner/.local/share/containers/storage tmpfs size=20G 0 0
EOF
)
