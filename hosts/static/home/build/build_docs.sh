#!/bin/bash
cd
if [ -d docs.git ]; then
	cd docs.git
	OLD_HEAD=$(git rev-parse HEAD)
	git pull origin main
	NEW_HEAD=$(git rev-parse HEAD)
else
	git clone https://codeberg.org/Codeberg/Documentation docs.git
	cd docs.git
	NEW_HEAD=$(git rev-parse HEAD)
	OLD_HEAD=""
fi

[ $OLD_HEAD = $NEW_HEAD ] && exit

ln -s /var/www/docs _site
npm install
npm run build
