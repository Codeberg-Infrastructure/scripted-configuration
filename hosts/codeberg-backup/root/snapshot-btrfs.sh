#!/usr/bin/env bash
unset LANG

DAY="$(date +%A)"
if [ -d "/codeberg-backup/snapshots/${DAY}" ]; then
	btrfs subvolume delete "/codeberg-backup/snapshots/${DAY}"
fi
btrfs subvolume snapshot "/codeberg-backup" "/codeberg-backup/snapshots/${DAY}"

