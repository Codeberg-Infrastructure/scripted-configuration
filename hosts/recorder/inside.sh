#!/usr/bin/env bash

set -ex

source "base/base.sh"


setup_ufw
ufw allow 80/tcp
ufw allow 8188/tcp # websockets
ufw allow 20000:40000/udp # rtp


# nginx & php
apt-get install -y --no-install-recommends nginx-light php-fpm php-mysql rsync apache2-utils

rm -f  /etc/nignx/sites-enabled/default
install_file_v2 "recorder" "/etc/nginx/sites-available/recorder" || systemctl reload nginx
if [ ! -f /etc/nginx/sites-enabled/recorder ]; then
	ln -s /etc/nginx/sites-available/recorder /etc/nginx/sites-enabled/recorder
	systemctl reload nginx
fi

# janus
apt-get install -y --no-install-recommends janus janus-demos
sed -i 's/#rtp_port_range = \"20000-40000\"/rtp_port_range = \"20000-40000\"/g' /etc/janus/janus.jcfg
sed -i 's/#nat_1_1_mapping = \"1.2.3.4\"/nat_1_1_mapping = \"217.197.91.161\"/g' /etc/janus/janus.jcfg

line_in_file "/usr/share/janus/demos/settings.js" "var token = \"\";"
line_in_file "/usr/share/janus/demos/settings.js" "var apisecret = \"\";"
sed -i 's/https/wss/g' /usr/share/janus/demos/settings.js

mkdir -p /var/www/recorder
ln -sf /usr/share/janus/demos /var/www/recorder/demos
