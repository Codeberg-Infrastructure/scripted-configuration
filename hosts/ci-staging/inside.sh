#!/usr/bin/env bash

set -ex

source "base/users.sh"

user_setup "pjs" "wheel"

dnf install -y openssh-server epel-release
systemctl enable sshd
