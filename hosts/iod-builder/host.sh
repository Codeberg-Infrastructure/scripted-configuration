#!/usr/bin/env bash

set -ex

VIRTUALIZATION=kvm
MEM=16
CPU=8
DISK_SIZE=128
