#!/usr/bin/env bash

INT_IFACE1="enp5s0f0"
INT_IFACE2="enp5s0f1"


set -ex
source "base/base.sh"
source "base/users.sh"

user_setup "ashimokawa" "sudo"
user_setup "otto" "sudo"
user_setup_jump

apt-get install -y smartmontools hdparm # disk management
apt-get install -y molly-guard etckeeper # for accidential mistakes 
apt_install_norecommends sysstat

# install backport kernel
# required for tmpfs with xattr (starting with ~6.6)
# can be removed later
enable_backports
apt-get install -y -t bookworm-backports linux-image-amd64

# networking
apt-get install -y firmware-bnx2x
apt-get install -y ifenslave
apt-get install -y chrony # NTP

install_file "achtermann" "/etc/hosts"
install_file "achtermann" "/etc/network/interfaces"
install_file "_machines" "/etc/network/interfaces.d/vip.conf"
install_file_v2 "_machines" "/etc/sysctl.d/10-codeberg.conf" || sysctl -p /etc/sysctl.d/10-codeberg.conf

setup_ufw
ufw allow 19198 # ssh
ufw allow in on lxcbr0
ufw route allow in on lxcbr0
ufw route allow out on lxcbr0
ufw allow in on $INT_IFACE1 to any port 53 # dns
ufw allow in on $INT_IFACE2 to any port 53 # dns
ufw allow in on wg-wobcom+ to any port 53 # dns
ufw allow in on $INT_IFACE1 to any port 3784 proto udp # bfd
ufw allow in on $INT_IFACE2 to any port 3784 proto udp # bfd
ufw allow in on $INT_IFACE1 to any port 6789 proto tcp # ceph mon
ufw allow in on $INT_IFACE2 to any port 6789 proto tcp # ceph mon
ufw allow in on $INT_IFACE1 to any port 3300 proto tcp # ceph mon v2
ufw allow in on $INT_IFACE2 to any port 3300 proto tcp # ceph mon v2
ufw allow in on $INT_IFACE1 to any port 6800:7300 proto tcp # ceph osd
ufw allow in on $INT_IFACE2 to any port 6800:7300 proto tcp # ceph osd
ufw allow in on $INT_IFACE1 to any port 3000 proto tcp # ceph grafana
ufw allow in on $INT_IFACE2 to any port 3000 proto tcp # ceph grafana
ufw allow in on $INT_IFACE1 to any port 8443 proto tcp # ceph dashboard
ufw allow in on $INT_IFACE2 to any port 8443 proto tcp # ceph dashboard
ufw allow in on $INT_IFACE1 to any port 9100 proto tcp # ceph node-exporter
ufw allow in on $INT_IFACE2 to any port 9100 proto tcp # ceph node-exporter
ufw allow in on $INT_IFACE1 to any port 9283 proto tcp # ceph prometheus exporter
ufw allow in on $INT_IFACE2 to any port 9283 proto tcp # ceph prometheus exporter
ufw allow in on $INT_IFACE1 to any port 8765 proto tcp # ceph cephadm discovery service
ufw allow in on $INT_IFACE2 to any port 8765 proto tcp # ceph cephadm discovery service
ufw allow in on $INT_IFACE1 to any port 9003:9005 proto tcp # ceph prometheus
ufw allow in on $INT_IFACE2 to any port 9003:9005 proto tcp # ceph prometheus

install_file_v2 "achtermann" "/etc/ufw/after.rules" || ufw reload

# dnsmasq
apt-get install -y dnsmasq

install_file "_machines" "/etc/resolv.conf"
install_file_v2 "_machines" "/etc/dnsmasq.d/codeberg.conf" || systemctl restart dnsmasq

# bird dynamic routing
apt-get install -y bird2
install_file_v2 "achtermann" "/etc/bird/bird.conf" || birdc configure
systemctl enable bird

# lxc
apt-get install -y lxc
install_file_v2 "achtermann" "/etc/default/lxc-net" || systemctl restart lxc-net

# ssh
install_file_v2 "achtermann" "/etc/ssh/sshd_config.d/codeberg.conf" || systemctl restart ssh

# fstab and mountpoints
mkdir -p /srv/ceph/data
install_file_v2 "achtermann" "/etc/fstab" || systemctl daemon-reload

# ceph dependecies
apt-get install -y podman lvm2
# ceph userspace tools
apt_install extrepo
extrepo enable ceph
apt-get update
apt-get install -y ceph-common cephadm

# module blacklist
install_file_v2 "achtermann" "/etc/modprobe.d/codeberg-blacklist.conf" || update-initramfs -u

# crypt unlock
apt-get install -y dropbear-initramfs
install_file "achtermann" "/etc/dropbear/initramfs/authorized_keys"
line_in_file "/etc/dropbear/initramfs/dropbear.conf" 'DROPBEAR_OPTIONS="-I 180 -j -k -p 2222 -s -c cryptroot-unlock"'

install_file "achtermann" "/etc/initramfs-tools/scripts/init-premount/bonding.sh"
install_file "achtermann" "/etc/initramfs-tools/scripts/init-bottom/bonding.sh"
line_in_file "/etc/initramfs-tools/initramfs.conf" "IP=127.0.0.1:::::lo:off" # Just a dummy to make dropbear want to start :/
line_in_file "/etc/initramfs-tools/modules" "bonding"

if confirm "Do you want to update initram now? (It might take a time and is not necessary on every deployment"; then
	update-initramfs -u
fi
