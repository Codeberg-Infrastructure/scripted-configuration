#!/usr/bin/env bash

set -ex

source "base/users.sh"

# ensure git has id 1001
if [ "$(id -u "git" 2>/dev/null)" == "" ]; then
	useradd --uid 1001 git
	mkdir -p /home/git
	chown git:git /home/git -R
fi

source "base/base.sh"
setup_sshd

user_grant "root" "otto"
user_grant "root" "ashimokawa"
user_grant "root" "gusted"
user_grant "root" "6543"
user_grant "root" "crystal"
user_grant "root" "algernon"
systemctl reload sshd

apt-get install -y --no-install-recommends git mariadb-client rsync

mkdir -p /data/git
mkdir -p /mnt/ceph-cluster
chown git:git /data/git /data/git/* || true # ignore if still empty
chown git:git /mnt/ceph-cluster /mnt/ceph-cluster/* || true
chmod o-rwx /data/git/*
chmod o-rwx /mnt/ceph-cluster/*
