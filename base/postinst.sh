#!/usr/bin/env bash
apt-get -yq install git

git clone https://codeberg.org/Codeberg-Infrastructure/scripted-configuration.git /root/scripted-configuration.git
cd /root/scripted-configuration.git
./inside.sh

echo "Installation complete..."
echo "Rebooting in 5s"
sleep 5
