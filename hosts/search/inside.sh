#!/usr/bin/env bash

set -ex

source "base/users.sh"

user_setup "meilisearch"
user_setup "otto" "sudo"
user_setup "ashimokawa" "sudo"


source "base/base.sh"
setup_sshd

source "base/secrets.sh"
 secret_get "MEILISEARCH_SECRET_KEY"
export MEILISEARCH_SECRET_KEY
# meilisearch
MEILISEARCH_VERSION="1.12.1"

if [ ! -f /root/meilisearch_${MEILISEARCH_VERSION}.deb ]; then
	wget https://github.com/meilisearch/meilisearch/releases/download/v${MEILISEARCH_VERSION}/meilisearch.deb -O /root/meilisearch_${MEILISEARCH_VERSION}.deb
fi

if  ! dpkg -l meilisearch | grep -q ${MEILISEARCH_VERSION}; then 
	dpkg -i  /root/meilisearch_${MEILISEARCH_VERSION}.deb
fi

install_template "search" "/etc/meilisearch.toml"

chown meilisearch:meilisearch /etc/meilisearch.toml
chmod 600 /etc/meilisearch.toml
mkdir -p /mnt/search-data/meilisearch
chown meilisearch:meilisearch /mnt/search-data/meilisearch
chmod 700 /mnt/search-data/meilisearch



install_file_v2 "search" "/etc/systemd/system/meilisearch.service" || systemctl enable meilisearch.service
systemctl start meilisearch.service

