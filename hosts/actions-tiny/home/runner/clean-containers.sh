#!/usr/bin/env bash
df -k | grep -E '/home/runner/.local/share/containers/storage$' | awk '{if ($3 > 15000000) system("podman system prune -f -a --filter until=720h")}'
