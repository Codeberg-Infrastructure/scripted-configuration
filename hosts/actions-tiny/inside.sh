#!/usr/bin/env bash

set -ex


source "base/base.sh"
setup_sshd

source "base/users.sh"
user_grant "root" "fnetx"
systemctl reload sshd

apt_install podman
apt_install_norecommends jq gpg

# podman setup
## store everything in tmpfs
install_file "actions-tiny" "/home/runner/.config/containers/storage.conf"
install_file "actions-tiny" "/home/runner/.config/containers/registries.conf"

## set environment for convenient interaction with podman
echo "export XDG_RUNTIME_DIR=/run/user/1101/" > /home/runner/.bashrc

## setup systemd service
source "base/systemd.sh"
systemd_service runner-podman

# runner setup

## user

if [ "$(id -u "runner" 2>/dev/null)" == "" ]; then
	useradd --uid 1101 runner
	mkdir -p /home/runner
	chown runner:runner /home/runner -R
fi
loginctl enable-linger runner
usermod --add-subuids 100000-165535 --add-subgids 100000-165535 runner

## runner installation

cd $(mktmp -d)
export LATEST_VERSION=$(curl -X 'GET' https://code.forgejo.org/api/v1/repos/forgejo/runner/releases/latest | jq .name -r | cut -c 2-)
export INSTALLED_VERSION=$(cat /usr/local/bin/forgejo-runner.version)
if [[ $LATEST_VERSION != "$INSTALLED_VERSION" ]]; then
	curl "https://code.forgejo.org/forgejo/runner/releases/download/v${LATEST_VERSION}/forgejo-runner-${LATEST_VERSION}-linux-amd64.xz" | xz -d > forgejo-runner
	chmod +x forgejo-runner
	wget -O forgejo-runner.asc "https://code.forgejo.org/forgejo/runner/releases/download/v${LATEST_VERSION}/forgejo-runner-${LATEST_VERSION}-linux-amd64.asc"
	gpg --keyserver keys.openpgp.org --recv EB114F5E6C0DC2BCDD183550A4B61A2DC5923710
	gpg --verify forgejo-runner.asc forgejo-runner
	rm forgejo-runner.asc
	mv forgejo-runner /usr/local/bin/forgejo-runner
	chmod +x /usr/local/bin/forgejo-runner
	echo "$LATEST_VERSION" > /usr/local/bin/forgejo-runner.version
	systemctl restart runner
fi
cd -

## runner config and installation

install_file "actions-tiny" "/home/runner/config.yml"
systemd_service runner

# temporary workaround: limit container storage size
install_file "actions-tiny" "/home/runner/clean-containers.sh"
systemd_timer "container size limit" "*-*-* *:38:52" "/home/runner/clean-containers.sh" "runner"

