#!/usr/bin/env bash

if [[ "$(hostname)" != "kampenwand" ]]; then
	echo "This must be executed on the host."
	exit 1
fi

set -ex
SECTION='<p id="custom">'
REASON=${1:-$(read -p "Please provide the reason for the maintenance. " && echo "$REPLY")}
sed -i "s|.*${SECTION}.*|${SECTION}<b>${REASON}</b></p><small>This maintenance page was last updated on $(date -u).</small>|" /etc/haproxy/error-pages/502.http
service haproxy reload
